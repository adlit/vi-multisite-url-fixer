# vi-multisite-url-fixer

WP plugin used for multisites based on https://github.com/roots/multisite-url-fixer

A change was made to the original code to allow the fixHomeURL function to add the /wp directory path even if the current site is not the main site or a sub-domain installation.